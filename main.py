#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time : 2022/1/1 下午 09:07
# @Author : Aries
# @Site : 
# @File : main.py
# @Software: PyCharm

from selenium import webdriver
from bs4 import BeautifulSoup
import urllib.request
import time
import os


def GetPicture(target, n):
    options = webdriver.ChromeOptions()
    options.add_argument('--headless')
    driver = webdriver.Chrome(options=options)
    driver.get(
        f"https://www.google.com/search?q={target}&rlz=1C1RXQR_zh-TWUS976US976&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjGveuA0ZD1AhW0r1YBHTl-DfoQ_AUoAXoECAIQAw&biw=1745&bih=852&dpr=1.1")
    picture_url = []
    pos = 0
    for i in range(n):
        html = driver.page_source
        soup = BeautifulSoup(html, 'html.parser')
        img_arr = soup.find_all('img')
        for img in img_arr:
            url = img.get('src')
            if url not in picture_url:
                picture_url.append(url)
        pos += i * 500
        js = "document.documentElement.scrollTop=%d" % pos
        driver.execute_script(js)
        time.sleep(1)
    driver.close()
    os.mkdir(target)
    for i in range(0, len(picture_url)):
        try:
            urllib.request.urlretrieve(picture_url[i], f'{target}/{i + 1}.jpg')
        except:
            print("第 " + str(i + 1) + " 張報錯")
    print("圖片儲存完成")


if __name__ == '__main__':
    GetPicture('鉛筆', 5)
